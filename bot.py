import os
import logging
import urllib
import discord
from discord.ext import commands
from lxml import html
import requests
from dotenv import load_dotenv

logger = logging.getLogger("discord")
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename="discord.log", encoding="utf-8", mode="w")
handler.setFormatter(logging.Formatter("%(asctime)s:%(levelname)s:%(name)s: %(message)s"))
logger.addHandler(handler)

# ###########################################################
# ##          nhentai unofficial discord bot v0.0.1        ##
# ###########################################################
# ##                  by oräkle (Ethan G.)                 ##
# ##     sources hosted on github.com/oraora/ (GPLv3)      ##
# ###########################################################

load_dotenv()
TOKEN = os.getenv("DISCORD_TOKEN")
bot = commands.Bot(command_prefix="!")
infoXpath = {
    "Artists": "/html/body/div[2]/div[1]/div[2]/div/section/div[4]/span/a",
    "Title": "//h1",
    "Categories": "/html/body/div[2]/div[1]/div[2]/div/section/div[7]/span/a",
    "Characters": "/html/body/div[2]/div[1]/div[2]/div/section/div[2]/span/a",
    "Parodies": "/html/body/div[2]/div[1]/div[2]/div/section/div[1]/span/a",
    "Tags": "/html/body/div[2]/div[1]/div[2]/div/section/div[3]/span/a",
    "Groups": "/html/body/div[2]/div[1]/div[2]/div/section/div[5]/span/a",
    "Language": "/html/body/div[2]/div[1]/div[2]/div/section/div[6]/span/a",
    "Thumbnail": "/html/body/div[2]/div[1]/div[1]/a/img"
}
baseUrl = "https://nhentai.net"

@bot.command(name="info", help="Commande pour récupérer les informations d'un hentai")
async def _info(ctx, arg):
    try:
        page = requests.get(f"{baseUrl}/g/{arg}")
        tree = html.fromstring(page.content)
        print(tree.xpath(f"{infoXpath['Artists']}/text()"))
        print(tree.xpath(infoXpath['Artists']))
        embed = discord.Embed(title=str(tree.xpath(f"{infoXpath['Title']}/text()"))[1:-1].replace("'", ""),
                              url=f"{baseUrl}/g/{arg}",
                              color=0xff0000)
        embed.set_image(url=tree.xpath(f"{infoXpath['Thumbnail']}/@data-src")[0])
        if len(tree.xpath(infoXpath['Artists'])) > 0:
            embed.add_field(name="Artists",
                            value=str(tree.xpath(f"{infoXpath['Artists']}/text()"))[1:-1].replace("'", ""),
                            inline=True)
        if len(tree.xpath(infoXpath['Groups'])) > 0:
            embed.add_field(name="Groups",
                            value=str(tree.xpath(f"{infoXpath['Groups']}/text()"))[1:-1].replace("'", ""),
                            inline=True)
        if len(tree.xpath(infoXpath['Language'])) > 0:
            embed.add_field(name="Language",
                            value=str(tree.xpath(f"{infoXpath['Language']}/text()"))[1:-1].replace("'", ""),
                            inline=True)
        if len(tree.xpath(infoXpath['Categories'])) > 0:
            embed.add_field(name="Categories",
                            value=str(tree.xpath(f"{infoXpath['Categories']}/text()"))[1:-1].replace("'", ""),
                            inline=True)
        if len(tree.xpath(infoXpath['Characters'])) > 0:
            embed.add_field(name="Characters",
                            value=str(tree.xpath(f"{infoXpath['Characters']}/text()"))[1:-1].replace("'", ""),
                            inline=True)
        if len(tree.xpath(infoXpath['Parodies'])) > 0:
            embed.add_field(name="Parodies",
                            value=str(tree.xpath(f"{infoXpath['Parodies']}/text()"))[1:-1].replace("'", ""),
                            inline=True)
        if len(tree.xpath(infoXpath['Tags'])) > 0:
            embed.add_field(name="Tags",
                            value=str(tree.xpath(f"{infoXpath['Tags']}/text()"))[1:-1].replace("'", ""),
                            inline=False)
        embed.set_footer(text=f"nhentai unofficial discord bot v{botVersion}")
        if (len(embed)) > 6000:
            embed.remove_field(len(embed.fields)-1)
            embedTags = discord.Embed(title=str(tree.xpath(f"{infoXpath['Title']}/text()"))[1:-1].replace("'", ""),
                                  url=f"{baseUrl}/g/{arg}",
                                  color=0xff0000)
            embedTags.add_field(name="Tags",
                                value=str(tree.xpath(f"{infoXpath['Tags']}/text()"))[1:-1].replace("'", ""),
                                inline=False)
            await ctx.send(embed=embed)
            await ctx.send(embed=embedTags)
        else:
            await ctx.send(embed=embed)
    except IndexError:
        await ctx.send("Cet ID n'existe pas.")
    except:
        await ctx.send("Erreur inattendue.")


@bot.command(name="search", help="Commande pour rechercher un hentai")
async def _search(ctx, *, arg):
    try:
        page = requests.get(f"{baseUrl}/search/?q={urllib.parse.quote_plus(arg)}")
        tree = html.fromstring(page.content)
        if str(tree.xpath("//h2/text()")) == "['No results found']":
            raise Exception("NotFound")
    except Exception as e:
        if str(e) ==  "NotFound":
            embed = discord.Embed(title=f"Search term: {arg}",
                                  url=f"{baseUrl}/search/?q={urllib.parse.quote_plus(arg)}",
                                  description="No results found.",
                                  color=0xff0000)
        else:
            embed = discord.Embed(title=str(e),
                                  description="An exception occured! Please contact the bot\'s developper.",
                                  color=0xff0000)
        await ctx.send(embed=embed)

    else:
        embed = discord.Embed(title=f"Search term: {arg}",
                              url=f"{baseUrl}/search/?q={urllib.parse.quote_plus(arg)}",
                              color=0xff0000)
        for i in range(0, len(tree.xpath("//div[@class='caption']/text()"))):
            hentai_id = tree.xpath("//a[@class='cover']/@href")
            embed.add_field(name=f"Result {i + 1} ({baseUrl}{hentai_id[i]})",
                            value=str(tree.xpath('//div[@class="caption"]/text()')[i]),
                            inline=False)
        await ctx.send(embed=embed)


@bot.event
async def on_ready():
    print('bot started')


bot.run(TOKEN)
